<h1 align="center">Daftcode – Recruitment task - Frontend (React) Intern at daftBlockchains</h1>

## 🔗 Live link

<p>Live Site URL: <a href="https://6283ee2488257d00970b2600--daftcode-task.netlify.app/">click here</a>.</p>

## 👨‍💼 The challenge

The goal of the task was to create a 10x10 board for the game of checkers and place one pawn on it. The pawn should move using the arrows on the keyboard. The game data is stored in the react context.

## 🏃‍♀️ How to run this project locally?

\*\* **You will need node and npm installed globally on your machine ([Node.js](https://nodejs.org/en/)).** \*\*

Clone this repo and install all required dependencies. Go into the project folder and type the following command to install all npm packages:

```
npm install
```

You can run the application - type the following command:

```
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

## 💻 Additional information
I used localStorage to store the position of the pawn. If the data is not stored then the default position will be used.