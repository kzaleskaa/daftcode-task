import { useEffect, useContext } from "react";
import BoardContext from "../store/board-context";

import classes from "./Pawn.module.css";

const Pawn = () => {
  const cartCtx = useContext(BoardContext);

  useEffect(() => {
    window.addEventListener("keydown", cartCtx.updatePosition);

    return () => {
      window.removeEventListener("keydown", cartCtx.updatePosition);
    };
  });

  return (
    <div
      className={classes.pawn}
      style={{
        left: cartCtx.position["x"],
        top: cartCtx.position["y"],
      }}
    ></div>
  );
};

export default Pawn;
