import classes from "./BoardContainer.module.css";

const BoardContainer = (props) => {
  return <div className={classes.container}>{props.children}</div>;
};

export default BoardContainer;
