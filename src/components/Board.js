import classes from "./Board.module.css";

const Board = () => {
  return (
    <>
      {[...Array(10)].map((value, indexCol) => (
        <div className={classes.column} key={indexCol}>
          {[...Array(10)].map((col, indexRow) => (
            <div className={classes.item} key={indexRow}></div>
          ))}
        </div>
      ))}
    </>
  );
};

export default Board;
