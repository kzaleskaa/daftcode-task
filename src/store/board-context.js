import React, { useEffect, useState } from "react";

const defaultState = { x: 8, y: 8 };

const BoardContext = React.createContext({
  position: {},
  updatePosition: (event) => {},
});

export const BoardContextProvider = (props) => {
  const [position, setPosition] = useState(defaultState);
  const positionRange = { maxPosition: 512, minPosition: 8 };
  const step = 56;

  useEffect(() => {
    const positionVal = JSON.parse(localStorage.getItem("position"));

    if (positionVal) {
      setPosition(positionVal);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("position", JSON.stringify(position));
  }, [position]);

  const updatePosition = (event) => {
    switch (event.key) {
      case "ArrowUp":
        if (position["y"] - step >= positionRange["minPosition"]) {
          setPosition((prevState) => ({
            ...prevState,
            y: prevState["y"] - step,
          }));
        }
        break;
      case "ArrowDown":
        if (position["y"] + step <= positionRange["maxPosition"]) {
          setPosition((prevState) => ({
            ...prevState,
            y: prevState["y"] + step,
          }));
        }
        break;
      case "ArrowLeft":
        if (position["x"] - step >= positionRange["minPosition"]) {
          setPosition((prevState) => ({
            ...prevState,
            x: prevState["x"] - step,
          }));
        }
        break;
      case "ArrowRight":
        if (position["x"] + step <= positionRange["maxPosition"]) {
          setPosition((prevState) => ({
            ...prevState,
            x: prevState["x"] + step,
          }));
        }
        break;
      default:
        break;
    }
  };

  const boardContext = {
    position: position,
    updatePosition: updatePosition,
  };

  return (
    <BoardContext.Provider value={boardContext}>
      {props.children}
    </BoardContext.Provider>
  );
};

export default BoardContext;
