import Board from "./components/Board";
import BoardContainer from "./components/BoardContainer";
import Pawn from "./components/Pawn";

function App() {
  return (
    <BoardContainer>
      <Board />
      <Pawn />
    </BoardContainer>
  );
}

export default App;
